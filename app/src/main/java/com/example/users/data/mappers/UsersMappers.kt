package com.example.users.data.mappers

import com.example.users.data.room.UserEntity
import com.example.users.domain.User
import com.example.users.domain.entity.response.ResultsEntity

fun ResultsEntity.toUserEntity(): UserEntity {
    return UserEntity(
        title = name.title,
        firstName = name.first,
        lastName = name.last,
        photoUrl = picture.medium,
        country = location.country,
        state = location.state,
        city = location.city,
        streetNumber = location.street.number,
        streetName = location.street.name,
        latitude = location.coordinates.latitude,
        longitude = location.coordinates.longitude,
        email = email,
        phoneNumber = phone
    )
}

fun UserEntity.toUser(): User {
   return User(
        title,
        firstName,
        lastName,
        photoUrl,
        country,
        state,
        city,
        streetNumber,
        streetName,
        latitude,
        longitude,
        email,
        phoneNumber
    )
}