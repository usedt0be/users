package com.example.users.data.dto.locationdto

import com.example.users.domain.entity.location.LocationEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LocationDto(
    override val street: StreetDto,
    @Json(name = "city")
    override val city: String,
    @Json(name = "state")
    override val state: String,
    @Json(name = "country")
    override val country: String,
    @Json(name = "postcode")
    override val postcode: String,
    @Json(name = "coordinates")
    override val coordinates: CoordinatesDto,
    @Json(name = "timezone")
    override val timezone: TimezoneDto,
): LocationEntity