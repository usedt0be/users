package com.example.users.data.repository

import android.util.Log
import com.example.users.data.mappers.toUserEntity
import com.example.users.domain.entity.repository.UsersRepository
import com.example.users.data.network.UserApiService
import com.example.users.data.room.UserDao
import com.example.users.data.room.UserEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
    private val userApi: UserApiService,

    ): UsersRepository {

    override suspend fun getUsersWithoutInternet(): Flow<List<UserEntity>> {
        return flow {
            emitAll(userDao.getAllUsers())
        }
    }

    override suspend fun refreshUsers(): Flow<List<UserEntity>> {
        val newUsers = userApi.getUsers().results
        userDao.clearAll()
        Log.d("newContacts", "$newUsers")
        userDao.upsertAll(users = newUsers.map {
            it.toUserEntity()
        })
        return flow {emitAll(userDao.getAllUsers())}
    }

    override suspend fun getUsersFromDb(): Flow<List<UserEntity>> {
        val preloadedUsers = userApi.getUsers().results
        Log.d("preloaded", "$preloadedUsers")
        return flow {
            Log.d("DB_state","${isDataBaseEmpty()}")
            if(isDataBaseEmpty()) {
                userDao.upsertAll(users = preloadedUsers.map {it.toUserEntity()})
                emitAll(userDao.getAllUsers())
            } else {
                emitAll(userDao.getAllUsers())
            }
        }.flowOn(Dispatchers.IO)
    }


    override suspend fun isDataBaseEmpty(): Boolean {
        val rowCount = userDao.getRowCount()
        return rowCount == 0
    }


}