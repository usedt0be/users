package com.example.users.data.dto.namedto


import com.example.users.domain.entity.name.NameEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NameDto(
    @Json(name = "title")
    override val title: String,
    @Json(name = "first")
    override val first: String,
    @Json(name = "last")
    override val last: String
): NameEntity
