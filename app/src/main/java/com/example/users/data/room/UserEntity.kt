package com.example.users.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Users")
data class UserEntity(
     @PrimaryKey(autoGenerate = true)
     val id : Long? = null,

     val title: String,
     val firstName: String,
     val lastName: String,
     val photoUrl: String?,
     val country:String,
     val state:String,
     val city:String,
     val streetNumber: Int,
     val streetName: String,
     val latitude: String,
     val longitude: String,
     val email:String,
     val phoneNumber: String
)