package com.example.users.data.network

import com.example.users.data.dto.responsedto.UsersResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApiService {
    @GET("api/")
    suspend fun getUsers(
        @Query("results") count:Int = 30
    ): UsersResponseDto

    companion object {
        const val BASE_URL = "https://randomuser.me/"
    }
}