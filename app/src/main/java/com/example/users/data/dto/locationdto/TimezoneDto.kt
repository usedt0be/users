package com.example.users.data.dto.locationdto

import com.example.users.domain.entity.location.TimezoneEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TimezoneDto(
    @Json(name = "offset")
    override val offset: String,
    @Json(name = "description")
    override val description: String
): TimezoneEntity
