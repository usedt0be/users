package com.example.users.data.dto.locationdto

import com.example.users.domain.entity.location.CoordinatesEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CoordinatesDto(
    @Json(name = "latitude")
    override val latitude: String,
    @Json(name = "longitude")
    override val longitude: String
): CoordinatesEntity