package com.example.users.data.dto.responsedto

import com.example.users.data.dto.locationdto.LocationDto
import com.example.users.data.dto.namedto.NameDto
import com.example.users.data.dto.picturedto.PictureDto
import com.example.users.domain.entity.response.ResultsEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResultsDto(
    @Json(name = "name")
    override val name: NameDto,
    @Json(name = "location")
    override val location: LocationDto,
    @Json(name= "email")
    override val email: String,
    @Json(name = "phone")
    override val phone: String,
    @Json(name = "picture")
    override val picture: PictureDto
): ResultsEntity
