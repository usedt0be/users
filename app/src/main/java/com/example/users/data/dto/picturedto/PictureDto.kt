package com.example.users.data.dto.picturedto

import com.example.users.domain.entity.picture.PictureEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PictureDto(
    @Json(name = "large")
    override val large: String,
    @Json(name = "medium")
    override val medium: String,
    @Json(name = "thumbnail")
    override val thumbnail: String
): PictureEntity
