package com.example.users.data.dto.locationdto

import com.example.users.domain.entity.location.StreetEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StreetDto(
    @Json(name = "number")
    override val number: Int,
    @Json(name = "name")
    override val name: String
): StreetEntity
