package com.example.contacts.data.network

import com.example.users.data.network.UserApiService
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitInstance {
    companion object {
        private val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(UserApiService.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()


        val userApi: UserApiService =
            retrofit.create(UserApiService::class.java)
    }
}