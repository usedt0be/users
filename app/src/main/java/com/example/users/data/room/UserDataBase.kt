package com.example.users.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [UserEntity::class], version = 2 , exportSchema = false)
abstract class UserDataBase: RoomDatabase() {
    abstract val usersDao: UserDao

    companion object {
        private var INSTANCE: UserDataBase? = null

        fun getInstance(context: Context): UserDataBase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context, UserDataBase:: class.java, "users")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return  INSTANCE as UserDataBase
        }
    }

}