package com.example.users.data.dto.responsedto

import com.example.users.domain.entity.response.UserResponseEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UsersResponseDto(
    @Json(name = "results")
    override val results: List<ResultsDto>
): UserResponseEntity
