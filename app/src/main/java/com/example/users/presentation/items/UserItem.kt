package com.example.users.presentation.items

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.users.domain.User
import com.example.users.ui.theme.DownloadedFonts


@Composable
fun UserItem(user: User, onClick:() -> Unit) {

    val fontFamily = DownloadedFonts.Lato.fontFamily

    Card(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 8.dp, end = 8.dp)
        .height(128.dp)
        .clickable { onClick() }
        .clip(RoundedCornerShape(20.dp)),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primary.copy(0.75f),
            contentColor = MaterialTheme.colorScheme.onPrimary)
    ) {
        Row(modifier = Modifier
            .padding(8.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically

        ) {
            Image(painter = rememberAsyncImagePainter(model = user.photoUrl),
                contentDescription = null,
                modifier = Modifier
                    .clip(CircleShape)
                    .size(90.dp)
                    .border(
                        width = 2.dp, color = MaterialTheme.colorScheme.onPrimary,
                        CircleShape
                    )
            )

            Column(modifier = Modifier
                .padding(start = 28.dp)
                .fillMaxHeight()
            ) {
                Text(text ="${user.title} ${user.firstName} ${user.lastName}",
                    modifier = Modifier.padding(top = 16.dp),
                    fontSize = 20.sp,
                    fontFamily = fontFamily,
                    color = MaterialTheme.colorScheme.onPrimary

                )
                Spacer(modifier = Modifier.height(16.dp))
                Text(text = user.country,
                    fontFamily = fontFamily,
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }
        }
    }
}



@Preview
@Composable
fun UserItemPreview() {
    UserItem(
        user = User(title = "Mr",
        firstName = "Robert",
        lastName = "Jackson",
        photoUrl = null,
        country = "USA",
        state = "Alabama",
        city = "Birmingham",
        streetNumber = 333,
        streetName = "Street",
        latitude = "124.5",
        longitude = "153.3",
        email = "@mail.com",
        phoneNumber = "+1450403593"
    ), onClick = {}
    )
}



