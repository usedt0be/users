package com.example.users.presentation.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.users.domain.usecases.RefreshUsersUseCase
import com.example.users.data.mappers.toUser
import com.example.users.data.repository.UsersRepositoryImpl
import com.example.users.domain.User
import com.example.users.domain.usecases.GetUsersFromDbUseCase
import com.example.users.domain.usecases.GetUsersWithoutInternetUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.UnknownHostException
import javax.inject.Inject


enum class STATE {
    LOADING,
    SUCCESS,
    FAILED
}

@HiltViewModel
class HomeViewModel @Inject constructor(
    repositoryImpl: UsersRepositoryImpl,

    ) : ViewModel() {
    private var refreshUsersUseCase = RefreshUsersUseCase(repositoryImpl)
    private var getUsersWithoutInternetUseCase =
        GetUsersWithoutInternetUseCase(repositoryImpl)
    private var getUsersFromDbUseCase = GetUsersFromDbUseCase(repositoryImpl)

    var state by mutableStateOf(STATE.LOADING)

    init {
        getUsers()
    }

    private val _selectedUserState: MutableState<User?> = mutableStateOf(null)
    val selectedUserState = _selectedUserState


    private val _usersDb = MutableStateFlow<List<User>>(emptyList())
    val usersDb = _usersDb.asStateFlow()


    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing = _isRefreshing

    private val _refreshingFailed = mutableStateOf(false)
    val refreshingFailed = _refreshingFailed
    private fun getUsers() {
        viewModelScope.launch {
            try {
                state = STATE.LOADING
                val resultFlow = getUsersFromDbUseCase.execute()
                Log.d("resflow", "$resultFlow")
                resultFlow.collect { result ->
                    Log.d("resList", "$result")
                    withContext(Dispatchers.Main) {
                        _usersDb.value = result.map { it.toUser() }

                    }
                    state = STATE.SUCCESS
                }
            } catch (e: IOException) {
                state = STATE.LOADING
                val resultFlow = getUsersWithoutInternetUseCase.execute()
                resultFlow.collect { result ->
                    withContext(Dispatchers.Main) {
                        _usersDb.value = result.map { it.toUser() }
                    }
                    state = STATE.SUCCESS
                }

            }

        }
    }


    fun refreshUsers() {
        viewModelScope.launch(Dispatchers.IO) {
            _refreshingFailed.value = false
            try {
                _isRefreshing.value = true
                val resultFlow = refreshUsersUseCase.execute()
                resultFlow.collect { result ->
                    Log.d("ref_bd", "$result")
                    withContext(Dispatchers.Main) {
                        _usersDb.value = result.map { it.toUser() }
                    }
                    _isRefreshing.value = false
                }

            } catch (e: UnknownHostException) {
                _isRefreshing.value = false
                _refreshingFailed.value = true
            }
        }
    }

    fun selectedUser(user: User) {
        _selectedUserState.value = user
    }

}