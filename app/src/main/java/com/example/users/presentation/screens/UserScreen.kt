package com.example.users.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.users.presentation.viewmodel.HomeViewModel
import com.example.users.ui.theme.DownloadedFonts
import com.example.users.presentation.util.Util

@Composable
fun UserScreen(onClickClose: () -> Unit, homeViewModel: HomeViewModel) {


    val contact = homeViewModel.selectedUserState.value!!
    val fontFamily = DownloadedFonts.Lato.fontFamily

    val context = LocalContext.current


    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background)
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.32f)
                .background(
                    color = MaterialTheme.colorScheme.primary,
                    shape = RoundedCornerShape(bottomStart = 48.dp, bottomEnd = 48.dp)
                )
        ) {
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 6.dp, top = 10.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
            ) {
                IconButton(onClick = {onClickClose()},
                    modifier = Modifier
                        .clip(CircleShape)
                        .background(MaterialTheme.colorScheme.onPrimary)

                ) {
                    Icon(imageVector = Icons.Rounded.ArrowBack,
                        contentDescription = "Back to home screen",
                        tint = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.size(18.dp)
                    )
                }
            }
            Row(
                modifier = Modifier
                    .padding(start = 18.dp, top = 56.dp)
                    .fillMaxWidth()
                    .height(150.dp), verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(model = contact.photoUrl),
                    contentDescription = "photo",
                    modifier = Modifier
                        .size(width = 132.dp, height = 132.dp)
                        .clip(CircleShape)
                        .border(
                            width = 2.dp,
                            color = MaterialTheme.colorScheme.onPrimary,
                            shape = CircleShape
                        )
                )
                Spacer(modifier = Modifier.width(20.dp))
                Box(
                    modifier = Modifier.fillMaxHeight(), contentAlignment = Alignment.TopStart
                ) {
                    Text(
                        text = "${contact.title} ${contact.firstName} ${contact.lastName}",
                        fontSize = 24.sp,
                        textAlign = TextAlign.Start,
                        modifier = Modifier.padding(top = 24.dp),
                        fontWeight = FontWeight.Bold,
                        fontFamily = fontFamily,
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        }
        Card(
            modifier = Modifier
                .padding(start = 24.dp, end = 24.dp)
                .fillMaxWidth()
                .height(90.dp)
                .offset(y = (-42).dp),
            colors = CardDefaults.cardColors
                (containerColor = MaterialTheme.colorScheme.onPrimary),
            shape = RoundedCornerShape(48.dp),
            elevation = CardDefaults.cardElevation(defaultElevation = 3.dp)
        )

        {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(start = 24.dp, end = 24.dp)
            ) {
                Box {
                    IconButton(onClick = {
                        Util.makePhoneCall(
                            phoneNumber = contact.phoneNumber,
                            context = context
                        )
                    }) {
                        Icon(
                            imageVector = Icons.Rounded.Phone,
                            contentDescription = "do phone call",
                            modifier = Modifier.size(32.dp),
                            tint = MaterialTheme.colorScheme.primary
                        )
                    }
                }

                Box {
                    IconButton(onClick = {
                       Util.sendEmail(context = context, email = contact.email)
                    }) {
                        Icon(
                            imageVector = Icons.Rounded.Email,
                            contentDescription = "send message",
                            modifier = Modifier.size(32.dp),
                            tint = MaterialTheme.colorScheme.primary
                        )

                    }

                }


                Box {
                    IconButton(onClick = {
                        Util.openMapsApp(
                            context = context,
                            latitude = contact.latitude.toDouble(),
                            longitude = contact.latitude.toDouble(),
                        )
                    }) {
                        Icon(
                            imageVector = Icons.Rounded.LocationOn,
                            contentDescription = "find location",
                            modifier = Modifier.size(32.dp),
                            tint = MaterialTheme.colorScheme.primary
                        )


                    }
                }

            }

        }


        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(start = 16.dp, end = 16.dp)
        ) {

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(80.dp)
                    .padding(top = 24.dp),
                colors = CardDefaults.cardColors(
                    containerColor = MaterialTheme.colorScheme.primary
                )
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize(),
                    contentAlignment = Alignment.CenterStart
                ) {
                    Row(
                        modifier = Modifier.padding(start = 8.dp),
                        horizontalArrangement = Arrangement.Start,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            imageVector = Icons.Rounded.Phone,
                            contentDescription = "phone number",
                            modifier = Modifier.size(32.dp),
                            tint = MaterialTheme.colorScheme.onPrimary
                        )

                        Text(
                            text = contact.phoneNumber,
                            modifier = Modifier
                                .padding(start = 12.dp)
                                .clickable {
                                    Util.makePhoneCall(
                                        phoneNumber = contact.phoneNumber,
                                        context = context
                                    )
                                }
                                .align(Alignment.CenterVertically),
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Normal,
                            fontFamily = fontFamily,
                            color = MaterialTheme.colorScheme.onPrimary
                        )
                    }
                }
            }


            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(80.dp)
                    .padding(top = 24.dp),
                colors = CardDefaults.cardColors(
                    containerColor = MaterialTheme.colorScheme.primary
                )
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize(),
                    contentAlignment = Alignment.CenterStart
                ) {
                    Row(
                        modifier = Modifier.padding(start = 8.dp),
                        horizontalArrangement = Arrangement.Start,
                    ) {
                        Icon(
                            imageVector = Icons.Rounded.Email,
                            contentDescription = "Email",
                            modifier = Modifier.size(32.dp),
                            tint = MaterialTheme.colorScheme.onPrimary
                        )

                        Text(
                            text = contact.email,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Normal,
                            fontFamily = fontFamily,
                            modifier = Modifier
                                .padding(start = 12.dp)
                                .align(Alignment.CenterVertically),
                            color = MaterialTheme.colorScheme.onPrimary
                        )

                    }
                }

            }



            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(100.dp)
                    .padding(top = 24.dp),
                colors = CardDefaults.cardColors(
                    containerColor = MaterialTheme.colorScheme.primary
                ),
                shape = RoundedCornerShape(12.dp)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize(),
                    contentAlignment = Alignment.CenterStart
                ) {
                    Row(
                        modifier = Modifier.padding(start = 8.dp),
                        horizontalArrangement = Arrangement.Start
                    ) {

                        Icon(
                            imageVector = Icons.Rounded.LocationOn,
                            contentDescription = "Address",
                            modifier = Modifier
                                .size(32.dp)
                                .align(Alignment.CenterVertically),
                            tint = MaterialTheme.colorScheme.onPrimary
                        )

                        Text(
                            text = "${contact.country}, ${contact.state}, ${contact.city}, " + "${contact.streetName} ${contact.streetNumber}",
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Normal,
                            fontFamily = fontFamily,
                            modifier = Modifier
                                .padding(start = 12.dp)
                                .align(Alignment.CenterVertically)
                                .clickable {
                                    Util.openMapsApp(
                                        context = context,
                                        latitude = contact.latitude.toDouble(),
                                        longitude = contact.latitude.toDouble(),
                                    )
                                },
                            color = MaterialTheme.colorScheme.onPrimary
                        )
                    }
                }
            }
        }
    }
}



//@Preview
//@Composable
//fun ContactScreenPreview() {
//    ContactScr(contact = ContactEntity(
//        id = null,
//        title = "Mr",
//        firstName = "Robert",
//        lastName = "Jackson",
//        photoUrl = null,
//        country = "USA",
//        state = "Alabama",
//        city = "Birmingham",
//        streetNumber = 333,
//        streetName = "Street",
//        latitude = "124.5",
//        longitude = "153.3",
//        email = "@mail.com",
//        phoneNumber = "+1450403593"
//    ), onClickClose = {}
//    )
//}



