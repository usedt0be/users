package com.example.users.presentation.screens

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.contentColorFor
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.users.domain.User
import com.example.users.presentation.items.UserItem
import com.example.users.presentation.viewmodel.HomeViewModel
import com.example.users.ui.theme.DownloadedFonts


@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun HomeScreen(homeViewModel: HomeViewModel, onClickUser: (User)-> Unit) {

    val users = homeViewModel.usersDb.collectAsState().value
    val fontFamily = DownloadedFonts.Lato.fontFamily
    Log.d("resulT", "$users")

    val refreshing by homeViewModel.isRefreshing.collectAsState()
    Log.d("ref" ,"$refreshing")

    val refreshingFailed = homeViewModel.refreshingFailed.value

    if(refreshingFailed) {
        Toast.makeText(
            LocalContext.current,
            "Update failed, check internet connection",
            Toast.LENGTH_SHORT)
            .show()
    }

    val refreshState = rememberPullRefreshState(refreshing = refreshing,
        onRefresh = {homeViewModel.refreshUsers()})


    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = "Users",
                        fontFamily = fontFamily
                    )
                },
                colors =  TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.background,
                    titleContentColor = MaterialTheme.colorScheme.onBackground )
            )
        },
    ) {scaffoldPadding ->
        Box(modifier = Modifier
            .fillMaxSize()
            .pullRefresh(refreshState)

        ) {

            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(scaffoldPadding),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                items(users) { user->
                    UserItem(user = user,
                        onClick = {
                            homeViewModel.selectedUser(user)
                            onClickUser(user)
                        }
                    )

                }
            }
            PullRefreshIndicator(
                refreshing = refreshing,
                state = refreshState,
                modifier = Modifier.align(Alignment.TopCenter),
                contentColor = contentColorFor(backgroundColor = MaterialTheme.colorScheme.primary)
            )

        }

    }

}