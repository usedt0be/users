package com.example.users.presentation.di



import android.app.Application
import com.example.contacts.data.network.RetrofitInstance
import com.example.users.data.repository.UsersRepositoryImpl
import com.example.users.data.room.UserDao
import com.example.users.data.room.UserDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Singleton
    @Provides
    fun provideUserRepositoryImpl(userDao: UserDao): UsersRepositoryImpl {
        return UsersRepositoryImpl(userDao = userDao, userApi = RetrofitInstance.userApi )
    }

    @Singleton
    @Provides
    fun provideUserDao(usersDataBase: UserDataBase): UserDao {
        return usersDataBase.usersDao
    }


    @Singleton
    @Provides
    fun provideUserDataBase(context: Application): UserDataBase {
        return UserDataBase.getInstance(context)
    }


}
