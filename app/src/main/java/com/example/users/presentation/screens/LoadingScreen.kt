package com.example.users.presentation.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.users.presentation.viewmodel.HomeViewModel
import com.example.users.presentation.viewmodel.STATE


@Composable
fun LoadingScreen(homeViewModel: HomeViewModel) {

    val navController = rememberNavController()

    when(homeViewModel.state) {
        STATE.FAILED -> ErrorSection()
        STATE.LOADING -> LoadingSection()
        STATE.SUCCESS -> {

            NavHost(navController = navController, startDestination = "homeScreen") {

                composable("homeScreen") {
                    HomeScreen(
                        homeViewModel = homeViewModel,
                        onClickUser = {
                            navController.navigate("userScreen")
                        })
                }
                composable("userScreen") {
                    UserScreen(homeViewModel = homeViewModel,
                        onClickClose = {
                            navController.navigate("homeScreen") {
                                popUpTo("homeScreen"){inclusive = true}
                            }
                        }
                    )
                }

            }

        }
    }

}

@Composable
fun LoadingSection() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }

}
@Composable
fun ErrorSection() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {
        Text(text = "Error", color = Color.Red)
    }
}


