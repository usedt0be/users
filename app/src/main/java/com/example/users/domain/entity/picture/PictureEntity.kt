package com.example.users.domain.entity.picture

interface PictureEntity {
    val large:String
    val medium:String
    val thumbnail:String
}