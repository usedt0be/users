package com.example.users.domain.entity.location

interface CoordinatesEntity {
    val latitude: String
    val longitude: String
}