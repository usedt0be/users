package com.example.users.domain

data class User(
    val title: String,
    val firstName: String,
    val lastName: String,
    val photoUrl: String?,
    val country: String,
    val state: String,
    val city: String,
    val streetNumber: Int,
    val streetName: String,
    val latitude: String,
    val longitude: String,
    val email: String,
    val phoneNumber: String
) {
}