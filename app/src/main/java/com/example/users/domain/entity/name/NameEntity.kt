package com.example.users.domain.entity.name

interface NameEntity {
    val title: String
    val first: String
    val last: String
}