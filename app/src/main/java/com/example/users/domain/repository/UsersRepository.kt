package com.example.users.domain.repository

import com.example.users.data.room.UserEntity
import kotlinx.coroutines.flow.Flow

interface UsersRepository {
    suspend fun getUsersFromDb() : Flow<List<UserEntity>>

    suspend fun isDataBaseEmpty(): Boolean

}