package com.example.users.domain.usecases


import com.example.users.domain.entity.repository.UsersRepository
import com.example.users.data.room.UserEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetUsersFromDbUseCase @Inject constructor(private val repository: UsersRepository) {
    suspend fun execute(): Flow<List<UserEntity>> {
        return repository.getUsersFromDb()
    }
}