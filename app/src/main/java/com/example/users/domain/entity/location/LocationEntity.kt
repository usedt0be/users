package com.example.users.domain.entity.location

interface LocationEntity {
    val street: StreetEntity
    val city: String
    val state: String
    val country: String
    val postcode: String
    val coordinates: CoordinatesEntity
    val timezone: TimezoneEntity
}