package com.example.users.domain.entity.location

interface StreetEntity {
    val number: Int
    val name: String
}