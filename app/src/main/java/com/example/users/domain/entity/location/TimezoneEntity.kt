package com.example.users.domain.entity.location

interface TimezoneEntity {
    val offset: String
    val description: String
}