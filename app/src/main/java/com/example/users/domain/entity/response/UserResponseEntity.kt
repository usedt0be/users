package com.example.users.domain.entity.response

interface UserResponseEntity {
    val results: List<ResultsEntity>
}