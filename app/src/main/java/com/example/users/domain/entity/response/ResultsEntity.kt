package com.example.users.domain.entity.response

import com.example.users.domain.entity.location.LocationEntity
import com.example.users.domain.entity.name.NameEntity
import com.example.users.domain.entity.picture.PictureEntity

interface ResultsEntity {
    val name: NameEntity
    val location: LocationEntity
    val email : String
    val phone: String
    val picture: PictureEntity
}