package com.example.users.domain.entity.repository

import com.example.users.data.room.UserEntity
import kotlinx.coroutines.flow.Flow

interface UsersRepository {

    suspend fun getUsersWithoutInternet() : Flow<List<UserEntity>>

    suspend fun getUsersFromDb() : Flow<List<UserEntity>>

    suspend fun refreshUsers(): Flow<List<UserEntity>>
    suspend fun isDataBaseEmpty(): Boolean

}